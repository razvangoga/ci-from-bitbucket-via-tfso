﻿using System.Web;
using System.Web.Mvc;

namespace ci_from_bitbucket_via_tfso
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
